import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { PetType } from 'src/entity/pettype.entity';
import { Repository } from 'typeorm';
/**
 * Providers can be injected into other classes via constructor parameter injection
 */
@Injectable()
export class PetTypeService {
  constructor(
    @InjectRepository(PetType)
    private petTypeRepository: Repository<PetType>,
  ) {}

  async addPetType(petType: PetType) {
    return await this.petTypeRepository.save(petType);
  }

  async getAllPetType(): Promise<PetType[]> {
    //return await this.petTypeRepository.find();
    return new Promise((res) => {
      setTimeout(() => {
        res(this.petTypeRepository.find());
      }, 3000);
    });
  }
}
