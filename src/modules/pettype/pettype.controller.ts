import { Body, CacheKey, Controller, Get, Post } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { PetType } from 'src/entity/pettype.entity';
import { PetTypeService } from './pettype.service';

/**
 * Receive request and produce response
 */
@ApiTags('petType')
@Controller('/petType')
export class PetTypeController {
  constructor(private readonly petTypeService: PetTypeService) {}

  @Post('/petType')
  async addPets(@Body() petType: PetType) {
    return await this.petTypeService.addPetType(petType);
  }

  @Get('/petType')
  @CacheKey('get_helper')
  async getAllPetType() {
    return await this.petTypeService.getAllPetType();
  }
}
