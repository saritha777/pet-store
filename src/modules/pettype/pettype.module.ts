import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PetType } from 'src/entity/pettype.entity';
import { PetTypeController } from './pettype.controller';
import { PetTypeService } from './pettype.service';

@Module({
  imports: [TypeOrmModule.forFeature([PetType])],
  controllers: [PetTypeController],
  providers: [PetTypeService],
})
export class PetTypeModule {}
