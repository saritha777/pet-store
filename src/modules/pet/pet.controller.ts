import {
  Controller,
  Get,
  Post,
  HttpException,
  HttpStatus,
  UseGuards,
  Body,
  Param,
} from '@nestjs/common';
import { PetService } from './pet.service';
import { ApiTags } from '@nestjs/swagger';
import { Pet } from 'src/entity/pet.entity';
import { Roles } from 'src/common/decorators/roles.decorator';
import { RolesGuard } from 'src/common/guards/roles.guard';
import { brotliDecompressSync } from 'zlib';
/**
 * controller that can receive in bound requests and produce reaponse
 */
@ApiTags('PETDATA')
@Controller('/petData')
export class PetController {
  constructor(private readonly petService: PetService) {}

  @Post('/pet/:id')
  @Roles('admin')
  @UseGuards(RolesGuard)
  addPet(@Param('id') id: number, @Body() pet: Pet): Promise<Pet> {
    return this.petService.addPet(id, pet);
  }

  @Get('/pet')
  getAllPets() {
    return this.petService.getAllPets();
  }

  @Get('/pet/:petName')
  getPetByName(@Param('petName') petName: string): Promise<Pet> {
    return this.petService
      .getPetByName(petName)
      .then((result) => {
        if (result) {
          return result;
        } else {
          throw new HttpException('pet not found', HttpStatus.NOT_FOUND);
        }
      })
      .catch(() => {
        throw new HttpException('pet not found', HttpStatus.NOT_FOUND);
      });
  }
}
