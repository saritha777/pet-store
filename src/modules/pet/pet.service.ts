import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Pet } from 'src/entity/pet.entity';
import { PetType } from 'src/entity/pettype.entity';
import { Repository } from 'typeorm';

@Injectable()
export class PetService {
  constructor(
    @InjectRepository(Pet)
    private petRepository: Repository<Pet>,
    @InjectRepository(PetType)
    private petTypeRepository: Repository<PetType>,
  ) {}

  async addPet(id: number, pet: Pet): Promise<Pet> {
    const petData = new Pet();
    const petTypeData: PetType = await this.petTypeRepository.findOne({
      petId: id,
    });
    petData.petType = pet.petType = petTypeData.petType;
    petData.petBreed = pet.petBreed;
    petData.petName = pet.petName;
    petData.colour = pet.colour;
    petData.price = pet.price;
    petData.isActive = pet.isActive;
    petData.createdBy = pet.createdBy;
    petData.createdDate = pet.createdDate;
    petData.updatedBy = pet.updatedBy;
    petData.updatedDate = pet.updatedDate;
    return this.petRepository.save(petData);
  }

  getAllPets(): Promise<Pet[]> {
    return this.petRepository.find();
  }

  getPetByName(petName: string): Promise<Pet> {
    return this.petRepository.findOne({ petName: petName });
  }

  async buyPet() {
    const petsData = await this.petRepository.find();
  }
}
