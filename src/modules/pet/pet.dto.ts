import { ApiProperty } from '@nestjs/swagger';
import { Maintainance } from 'src/entity/maintainance.entity';

export class PetDto extends Maintainance {
  @ApiProperty()
  petName: string;

  @ApiProperty()
  petBreed: string;

  @ApiProperty()
  colour: string;

  @ApiProperty()
  price: number;
}
