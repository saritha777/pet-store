import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { TypeOrmModule } from '@nestjs/typeorm';
import { JwtToken } from 'src/common/providers/jwtservice/jwttoken';
import { Register } from 'src/entity/Register.entity';
import { User } from 'src/entity/User.entity';
import { UserController } from './user.controller';
import { UserService } from './user.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([User, Register]),
    JwtModule.register({
      secret: 'secret',
      signOptions: { expiresIn: '10hrs' },
    }),
  ],
  controllers: [UserController],
  providers: [UserService, JwtToken],
})
export class UserModule {}
