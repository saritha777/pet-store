import {
  Body,
  Controller,
  Get,
  HttpException,
  HttpStatus,
  Post,
  Req,
  Res,
  UseGuards,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Register } from 'src/entity/Register.entity';
import { User } from 'src/entity/User.entity';
import { UserService } from './user.service';
import { Response, Request } from 'express';
import { Roles } from 'src/common/decorators/roles.decorator';
import { RolesGuard } from 'src/common/guards/roles.guard';

/**
 * controller that can receive inbound requests and produce responses
 */
@ApiTags('USER')
@Controller('/UserData')
export class UserController {
  constructor(private readonly userService: UserService) {}
  /**
   * we add all the user details by passing user
   * @param user user taken as parameter
   * @returns added user details
   */
  @Post('/logindata')
  async addUser(@Body() user: User): Promise<User> {
    return await this.userService.addUser(user);
  }
  /**
   *we add login details of user by passing below params as input
   * @param login we use login entity
   * @param response we get response by posting login
   * @param request we give request
   * @returns login value with request and redsponse
   */
  @Post('/login')
  async login(
    @Body() login: Register,
    @Res({ passthrough: true }) response: Response,
    @Req() request: Request,
  ) {
    return await this.userService
      .login(login, response, request)
      .then((result) => {
        if (result) {
          return result;
        } else {
          throw new HttpException('user not found', HttpStatus.NOT_FOUND);
        }
      })
      .catch(() => {
        throw new HttpException('user not found', HttpStatus.NOT_FOUND);
      });
  }
  /**
   * here we declare get method for returning all details of user
   * @returns get all details of user
   */
  @Get('/getAll')
  // @Roles('admin')
  // @UseGuards(RolesGuard)
  async getAll() {
    return this.userService
      .getAll()
      .then((result) => {
        if (result) {
          return result;
        } else {
          throw new HttpException('user not found', HttpStatus.NOT_FOUND);
        }
      })
      .catch(() => {
        throw new HttpException('user not found', HttpStatus.NOT_FOUND);
      });
  }
}
