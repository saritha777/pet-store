import {
  Injectable,
  BadRequestException,
  UnauthorizedException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { JwtToken } from 'src/common/providers/jwtservice/jwttoken';
import { Register } from 'src/entity/Register.entity';
import { User } from 'src/entity/User.entity';
import { Repository } from 'typeorm';
import * as bcrypt from 'bcryptjs';
import { Request, Response } from 'express';
import { globalAccess } from 'src/common/constants/globalAccess';

/**
 * Providers can be injected into other classes via constructor parameter injection
 */
@Injectable()
export class UserService {
  [x: string]: any;
  /**
   *all repositories is injected in constructor
   * @param userRepository user is injected by user repository
   * @param loginRepository Reg is injected by login repository
   * @param jwtToken jwtToken is instance of userservice and returns jwtToken
   */
  constructor(
    @InjectRepository(User)
    private userRepository: Repository<User>,
    @InjectRepository(Register)
    private loginRepository: Repository<Register>,
    private jwtToken: JwtToken,
  ) {}

  /**
   * we declare adduser method to add some details of user by passing data as input
   * @param data user as param
   * @returns userData
   */
  async addUser(data: User): Promise<User> {
    const pass = await bcrypt.hash(data.password, 10);
    const userData = new User();
    const loginData = new Register();
    userData.updatedDate = data.updatedDate;
    userData.updatedBy = data.updatedBy;
    userData.phoneNumber = data.phoneNumber;
    userData.password = loginData.password = pass;
    userData.isActive = data.isActive;
    userData.email = loginData.email = data.email;
    userData.customerName = data.customerName;
    userData.createdBy = data.createdBy;
    userData.createdDate = data.createdDate;
    userData.role = data.role;
    userData.register = loginData;
    return this.userRepository.save(userData);
  }
  /**
   * wre declare getAll method for returning all details of user
   * @returns getting all details of user
   */
  async getAll(): Promise<User[]> {
    return this.userRepository.find();
  }

  /**
   * we declare this method for checking user is logged in or not
   * @param data pass Register as input
   * @param response pass  as input  as input
   * @param req pass request  as input
   * @returns token id details
   */
  async login(data: Register, response: Response, req: Request) {
    const user = await this.loginRepository.findOne({
      email: data.email,
    });
    console.log(user);
    if (!user) {
      throw new BadRequestException('invalid credentials');
    }
    if (!(await bcrypt.compare(data.password, user.password))) {
      throw new BadRequestException('invalid ');
    }
    const jwt = await this.jwtToken.generateToken(user);
    response.cookie('jwt', jwt, { httpOnly: true });
    console.log('printing');
    const cookie = req.cookies['jwt'];
    globalAccess.role = user.user.role;
    const a = await this.jwtToken.verifyToken(cookie);
    return a;
  }
  getUserByEmail(email: string) {
    return this.userRepository.findOne({ email: email });
  }

  /* async getRoleByUserId(request: Request) {
    const cookie = request.cookies['jwt'];
    const data = await this.jwtToken.verifyToken(cookie);
    console.log('user data: ', data);
    if (!data) {
      console.log('if loop');
      throw new UnauthorizedException();
    }
    const user = await this.userLogin({ id: data['id'] });
    const { password, ...result } = user;
    globalAccess.role = result.role;
    return result.role;
  }

  async userLogin(condition: any) {
    return this.userRepository.findOne(condition);
  } */
}
