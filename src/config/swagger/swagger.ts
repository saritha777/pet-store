import { INestApplication } from '@nestjs/common';

import { OpenAPIObject, DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

import { SWAGGER_CONFIG } from './swagger.config';
/**
 * createdocument export app and options
 * @param app of INestApplication is added
 * @returns app and options
 */
export function createDocument(app: INestApplication): OpenAPIObject {
  /**
   * the swagger details like title descrption,version,tags to transfer to swagger_config
   */
  const builder = new DocumentBuilder()

    .setTitle(SWAGGER_CONFIG.title)

    .setDescription(SWAGGER_CONFIG.description)

    .addBasicAuth(
      { type: 'http', scheme: 'bearer', bearerFormat: 'JWT' },
      'access-token'
    )

    .setVersion(SWAGGER_CONFIG.version);
  /**
   * the swagger details like title descrption,version,tags to transfer to swagger_config
   */
  for (const tag of SWAGGER_CONFIG.tags) {
    builder.addTag(tag);
  }

  const options = builder.build();

  return SwaggerModule.createDocument(app, options);
}
