/**
 * interface:swagger
 */
export interface SwaggerConfig {
  /**
   * the name of the swagger and its details
   */
  title: string;
  /**
   * description of the swagger
   */
  description: string;
  /**
   * version of the swagger
   */
  version: string;
  /**
   * tags of the swagger
   */
  tags: string[];
}
