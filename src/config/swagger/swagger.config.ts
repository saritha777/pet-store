import { SwaggerConfig } from './swagger.interface';
/**
 * the swagger details like title descrption,version,tags to transfer to swagger_config
 */
export const SWAGGER_CONFIG: SwaggerConfig = {
  title: 'PET STORE',

  description: 'pet store',

  version: '1.0',

  tags: ['Template'],
};
