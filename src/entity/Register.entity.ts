import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';
import {
  Column,
  Entity,
  JoinColumn,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { User } from './User.entity';

/**
 * Casting typeorm to entity
 */
@Entity()
export class Register {
  @PrimaryGeneratedColumn()
  userId: number;

  @ApiProperty()
  @IsString()
  @Column()
  email: string;

  // @Column()
  // role: string;

  @ApiProperty()
  @IsString()
  @Column()
  password: string;

  @OneToOne(() => User, (user) => user.register, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  @JoinColumn()
  user: User;
}
