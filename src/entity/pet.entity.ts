import { ApiProperty } from '@nestjs/swagger';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Maintainance } from './maintainance.entity';
import { PetType } from './pettype.entity';

@Entity()
export class Pet extends Maintainance {
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty()
  @Column()
  petName: string;

  @ApiProperty()
  @Column()
  petBreed: string;

  @Column()
  petType: string;

  @ApiProperty()
  @Column()
  colour: string;

  @ApiProperty()
  @Column()
  price: number;

  @ManyToOne(() => PetType, (pettype) => pettype.pet, {
    cascade: true,
  })
  @JoinColumn()
  pettype: PetType;
}
