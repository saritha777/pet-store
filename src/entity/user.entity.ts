import { ApiProperty } from '@nestjs/swagger';
import { Column, Entity, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Maintainance } from './maintainance.entity';
import { Register } from './Register.entity';

@Entity()
export class User extends Maintainance {
  @PrimaryGeneratedColumn()
  userId: number;

  @ApiProperty({
    type: String,
    description: 'The  userName of the user',
    default: '',
  })
  @Column()
  customerName: string;

  @ApiProperty({
    type: String,
    description: 'The  email of the user',
    default: '',
  })
  @Column({ unique: true })
  email: string;

  @ApiProperty({
    type: String,
    description: 'The  password of the user',
    default: '',
  })
  @Column()
  password: string;

  @ApiProperty({
    type: String,
    description: 'The  role of the user',
    default: '',
  })
  @Column()
  role: string;

  @ApiProperty({
    type: String,
    description: 'The  phoneNumber of the user',
    default: '',
  })
  @Column()
  phoneNumber: string;

  @OneToOne(() => Register, (register) => register.user, {
    cascade: true,
  })
  register: Register;
}
