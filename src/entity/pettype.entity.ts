import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { ApiProperty } from '@nestjs/swagger';
import { Pet } from './pet.entity';
@Entity()
export class PetType {
  @PrimaryGeneratedColumn()
  petId: number;

  @ApiProperty()
  @Column()
  petType: string;

  @OneToMany(() => Pet, (pet) => pet.pettype)
  pet: Pet;
}
