import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';
/**
 * Receive request and produce response
 */
@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}
  /**
   * This function gets getHello()
   * @returns the string value
   */
  @Get()
  getHello(): string {
    return this.appService.getHello();
  }
}
