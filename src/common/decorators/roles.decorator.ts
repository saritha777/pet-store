import { SetMetadata } from '@nestjs/common';
/**
 * it exports roles data
 * @param role is added
 * @returns role metadata
 */
export const Roles = (role) => SetMetadata('roles', role);
