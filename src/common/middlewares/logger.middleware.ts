import { Injectable, Logger, NestMiddleware } from '@nestjs/common';
import { NextFunction, Request, Response } from 'express';
/**
 * Middleware is a function which is called before the route handler
 */
@Injectable()
export class LoggerMiddleware implements NestMiddleware {
  /**
   * 
     * Logger instance
     
   */
  private logger = new Logger('MovieTickets-Booking');
  /**
   * tranfer to nest routehandler
   * @param request is added
   * @param response is added
   * @param next is used to recuring the functin with out stop
   */
  use(request: Request, response: Response, next: NextFunction): void {
    const { ip, method, originalUrl } = request;
    const userAgent = request.get('user-agent') || '';

    response.on('finish', () => {
      const { statusCode } = response;
      const contentLength = response.get('content-length');
      this.logger.log(
        `${method} ${originalUrl} ${statusCode} ${contentLength}-${userAgent}${ip}`,
      );

      if (method !== 'GET') {
        this.logger.error(request.body);
      }
    });

    next();
  }
}
