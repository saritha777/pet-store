import {
  PipeTransform,
  Injectable,
  ArgumentMetadata,
  BadRequestException,
} from '@nestjs/common';

import { validate } from 'class-validator';

import { plainToClass } from 'class-transformer';
/**
 * Providers can be injected into other classes via constructor parameter injection
 */
@Injectable()
export class ValidationPipes implements PipeTransform<any> {
  /**
   * by using transform method it returns value
   * @param value param as value return as any
   * @param param1 param as metatype return argumentmetadata
   * @returns value
   */
  async transform(value: any, { metatype }: ArgumentMetadata) {
    if (!metatype || !this.toValidate(metatype)) {
      return value;
    }

    const object = plainToClass(metatype, value);

    const errors = await validate(object);

    if (errors.length > 0) {
      throw new BadRequestException('Validation failed');
    }

    return value;
  }
  /**
   * by using tovalidate method it returns paticular type
   * @param metatype param as metatype returns as string
   * @returns type
   */
  private toValidate(metatype: any): boolean {
    const types: any[] = [String, Boolean, Number, Array, Object];

    return !types.includes(metatype);
  }
}
