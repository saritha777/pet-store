import { Injectable, Res, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { Response } from 'express';
import { Register } from 'src/entity/Register.entity';

/**
 * for actvating security feature we use jwttoken
 */
@Injectable()
export class JwtToken {
  // verifyAsync(cookie: any) {
  //   throw new Error('Method not implemented.');
  // }
  /**
   *
   * @param jwtService
   * jwt authenticatin for securing from others
   */
  constructor(private jwtService: JwtService) {}
  /**
   *
   * @param data
   * @returns login
   * generate token when succefully login
   */
  async generateToken(data: Register) {
    const jwt = await this.jwtService.signAsync({ id: data.userId });
    return jwt;
  }
  /**
   *
   * @param token
   * @returns string
   * verifying token for authentication
   */
  async verifyToken(token: string) {
    const data = await this.jwtService.verifyAsync(token);
    if (!data) {
      throw new UnauthorizedException();
    }
    return data;
  }
  /**
   *
   * @param response
   * @returns boolean
   * deletetoken whenever we requires
   */
  async deleteToken(@Res({ passthrough: true }) response: Response) {
    response.clearCookie('jwt');
    return {
      message: 'logout',
    };
  }
}
