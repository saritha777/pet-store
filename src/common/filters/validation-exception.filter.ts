import {
  ExceptionFilter,
  Catch,
  ArgumentsHost,
  HttpException,
  BadRequestException,
} from '@nestjs/common';

import { Request, Response } from 'express';
/**
 * to caught the exceptins
 */
@Catch(BadRequestException)
export class ValidationExceptionFilter implements ExceptionFilter {
  /**
   * writing catch method to caught the exceptions
   * @param exception of HttpException is added
   * @param host of ArgumentsHost is added
   */
  catch(exception: HttpException, host: ArgumentsHost) {
    const ctx = host.switchToHttp();

    const response = ctx.getResponse<Response>();

    const request = ctx.getRequest<Request>();

    const status = exception.getStatus();

    response

      .status(status)

      .json({
        statusCode: status,

        timestamp: new Date().toISOString(),

        path: request.url,

        description: 'Bad Request!!!',
      });
  }
}
