import {
  ExceptionFilter,
  Catch,
  ArgumentsHost,
  HttpException,
} from '@nestjs/common';
import { Request, Response } from 'express';

/**
 * writting catch method to cought the exceptions
 */
@Catch(HttpException)
/**
 * writting a class for http exception filter
 */
export class HttpExceptionFilter implements ExceptionFilter {
  /**
   * writting catch methot to cought
   * @param exception passing exception as input
   * @param host passing as imput
   */

  catch(exception: HttpException, host: ArgumentsHost) {
    const ctx = host.switchToHttp();

    const response = ctx.getResponse<Response>();

    const request = ctx.getRequest<Request>();

    const status = exception.getStatus();

    response.status(status).json({
      statusCode: status,

      timestamp: new Date().toISOString(),

      path: request.url,

      description: 'Http error',
    });
  }
}
