import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';
import { ValidationExceptionFilter } from './common/filters/validation-exception.filter';
import { ValidationPipes } from './common/pipe/validation.pipe';
import { createDocument } from './config/swagger/swagger';
import * as cookieParser from 'cookie-parser';
/**
 * starting point of the Application
 */
async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  //app.useGlobalFilters( new HttpExceptionFilter());

  app.useGlobalPipes(new ValidationPipes());

  app.useGlobalFilters(new ValidationExceptionFilter());

  app.useGlobalPipes(new ValidationPipe({ transform: true }));

  app.setGlobalPrefix('api/v1');

  SwaggerModule.setup('api', app, createDocument(app));

  app.use(cookieParser());
  app.enableCors({
    origin: 'http://localhost:8080',
    credentials: true,
  });

  await app.listen(3000);
}
bootstrap();
