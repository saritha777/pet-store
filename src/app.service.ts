import { Injectable } from '@nestjs/common';
/**
 * Having all business logics related to AppService
 */
@Injectable()
export class AppService {
  /**
   * stores the string value in getHello()
   * @returns the string value
   */
  getHello(): string {
    return 'Hello World!';
  }
}
